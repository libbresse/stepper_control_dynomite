// Stegmotorstyrning till Dynomite-bänk. MCT6 optocopplare används till insignalen med tillhörande resistorer


// constants won't change. They're used here to 
// set pin numbers:
const int inputDir1 = 2;     // the number of the pushbutton pin
const int inputDir2 = 3;
const int inputTest1 = 4;
const int inputTest2 = 5;
const int stepPin =  11;      // the number of the LED pin
const int dirPin =  12;  
const int breakPin =  13;  
// variables will change:
int buttonState1 = 0;         // variable for reading the pushbutton status
int buttonState2 = 0;         // variable for reading the pushbutton status
int buttonTest1=0;
int buttonTest2=0;
long iCount =0;

void setup() {
  // initialize the LED pin as an output:
  pinMode(stepPin, OUTPUT);      // stegpinne
  pinMode(dirPin, OUTPUT);       
  pinMode(breakPin, OUTPUT); 
  // initialize the pushbutton pin as an input:
  pinMode(inputDir1, INPUT);  // insignal riktning och steg
  pinMode(inputDir2, INPUT);
  pinMode(inputTest1, INPUT);  // testkörning av stegmotor
  pinMode(inputTest2, INPUT);
  digitalWrite(inputDir1, HIGH);
  digitalWrite(inputDir2, HIGH);
  digitalWrite(inputTest1, HIGH);
  digitalWrite(inputTest2, HIGH);
}

void loop(){
  // read the state of the pushbutton value:
  buttonState1 = digitalRead(inputDir1);
  buttonState2 = digitalRead(inputDir2);
  buttonTest1 = digitalRead(inputTest1);
  buttonTest2 = digitalRead(inputTest2);
  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState1 == LOW) {     
    // turn LED on:    
    digitalWrite(dirPin, HIGH);
    digitalWrite(stepPin, HIGH);
    digitalWrite(breakPin, LOW);
    //delay(1);
    iCount = 0;
  } 
  else if(buttonState2 == LOW){
    digitalWrite(dirPin, LOW);
    digitalWrite(stepPin, HIGH);
    digitalWrite(breakPin, LOW);
    //delay(1);
    iCount = 0;
  }
  else if (buttonTest1 == LOW) {     
    digitalWrite(dirPin, HIGH);
    digitalWrite(stepPin, HIGH);
    digitalWrite(breakPin, LOW);
    delay(30);
    digitalWrite(dirPin, HIGH);
    digitalWrite(stepPin, LOW);
    digitalWrite(breakPin, LOW);
    delay(30);
    iCount = 0;
  } 
  else if(buttonTest2 == LOW){
    digitalWrite(dirPin, LOW);
    digitalWrite(stepPin, HIGH);
    digitalWrite(breakPin, LOW);
    delay(30);
    digitalWrite(dirPin, LOW);
    digitalWrite(stepPin, LOW);
    digitalWrite(breakPin, LOW);
    delay(30); 
    iCount = 0;    
  }
  else{
  
    digitalWrite(stepPin, LOW);
    if(iCount>100000)  {            // slå av bromsen om ingen signal kommit in på x antal loopar
      digitalWrite(breakPin,HIGH); 
    }
    else{
      iCount++;
    }
    
  }
}

